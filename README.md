# README #

ReverseWordsInSentence is a console application which accepts the sentence as an input and reverse each word in a given sentence. 

## ReverseWordsInSentence Repository 
This repository is to reverse words in sentence . 

### How do I get set up? ###

* Download ReverseWordsInSentence solution and build and run the console application 
* There are two projects in the solution
1. ReverseSentence -> This project has the source code 
2. TestReverseSentence -> This is a test project 


