﻿using System;
using System.Linq;
using static System.Console;


namespace ReverseSentence

{
    /// <summary>
    /// 
    /// This below code take the sentence as an input and reverse the characters of the given string  
    /// </summary>
    public static class Program

    {

        static void Main(string[] args)

        {
            ForegroundColor = ConsoleColor.White;

            WriteLine("Enter a String:");

            ForegroundColor = ConsoleColor.Yellow;

            string s = ReadLine();

            if (s != null)
            {

                ForegroundColor = ConsoleColor.Red;

                WriteLine("Reverse String is:");


                string result = GetStringReversed(s);

                ForegroundColor = ConsoleColor.White;
                Write(result);

            }

            ReadKey();

        }
        /// <summary>
        /// GetStringReversed - Function to reverse the input string 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>

        public static string GetStringReversed(string s)
        {
            //check for null string 
            if (s == null) throw new ArgumentNullException(nameof(s));
            char[] punctuation = new char[] { ' ', ',', '.', ':', '\t', '?', '!', '@', '#' };
            char[] inputAsChars = s.ToCharArray();
            var result = string.Empty;

            var temp = "";
            foreach (var c in inputAsChars)
            {
                if (punctuation.Contains(c))
                {

                    char[] tx = temp.ToCharArray();
                    Array.Reverse(tx);
                    temp = new string(tx);
                    result += temp + c;
                    temp = "";
                }
                else
                    temp += c;
            }
            return result;


        }

    }

}


