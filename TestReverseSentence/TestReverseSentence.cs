﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReverseSentence;
using FluentAssertions;

namespace TestReverseSentence
{
    [TestClass]
    public class TestReverseSentence
    {
        [TestMethod]
        public void SucessfullyReverseSentence()
        {
           string result = Program.GetStringReversed("This is test.");
            result.Should().BeEquivalentTo("sihT si tset.");

        }
      
}
}
